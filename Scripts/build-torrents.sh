#!/bin/bash
 
# This is the default on the SIG machine, feel free to adjust to your environment
repodir=/srv/livecds/$2
 
#for spin in CINN KDE LXDE MATE WORK SOAS XFCE; do
for spin in CINN KDE LXDE LXQT MATE WORK SOAS XFCE; do 
#for spin in CINN KDE LXDE LXQT MATE WORK SECURITY SOAS XFCE; do 
   hash=$(grep $spin $repodir/CHECKSUM512-$2)
    transmission-create -c "ISO SHA512SUM: ${hash} " -s 2048 -p -t http://respins01.fedorainfracloud.org:6969/announce -o $repodir/F$1-${spin}-x86_64-$2.torrent $repodir/F$1-${spin}-x86_64-$2.iso
    chmod +r $repodir/*.torrent
 
    echo "update opentracker whitelist for ${spin}"
    transmission-show $repodir/F$1-${spin}-x86_64-$2.torrent | awk '/Hash/{print $2 " - "}' >> $repodir/buildfile
    echo F$1-${spin}-x86_64-$2.iso >> $repodir/HASHSUM512-$2
done
# For environments with an onboard private (closed) instance of Opentracker the following copies the HASHSUM file to the default Opentracker Whitelist file.
# cat $repodir/HASHSUM512-$2 | paste -d "" - - > /var/opentracker/whitelist

# If you no longer need the buildfile in the present directory or rely on parsing the transfered whitelist, uncomment the following. 
# /bin/rm $repodir/HASHSUM512-$2 

