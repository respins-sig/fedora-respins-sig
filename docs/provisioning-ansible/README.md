Provisioning a New Builder
==========================

These playbooks are used to provision new builders.

Manual Linode Process to Login and Move to Fedora Kernel
--------------------------------------------------------
Setup a few things on the node to use the Fedora provided kernel and enable SELinux.

### Control the Boot Process
Disable Lassie booting in the Linode console. Login and ensure the node will be able to PVGRUB and set the hostname:

```shell
ssh root@$loc.fspin.org
hostnamectl set-hostname $loc.fspin.org
dnf -y install kernel-core grub2 ansible libselinux-python
shutdown -h now
```

### Change the Kernel in Linode to GRUB 2
Go into the configuration profile and change the kernel to boot to GRUB 2. Connect to the weblish and then boot and interrupt boot. Add the following runtime kernel options:

```
enforcing=0 autorelabel=1
```

This will auto-reboot, resulting with an offline node. Enable Lassie booting. Boot the Linode.

### Run Ansible Initial Configuration
Run the initial configuration as `root` to create users and install base programs. After running this playbook, the `root` user will no longer work.

Run the playbook as `root`:

```shell
ansible-playbook -u root -l $loc --ask-pass playbooks/initial_base_setup.yml
```

### Future Playbook Runs
Once a node is provisioned, you need to login as $user and set a password.

```shell
ssh $user@$loc.fspin.org
[$user@$loc ~]$ passwd
```

For further playbook runs, use the following syntax:
```shell
ansible-playbook -u $user -l $loc --ask-sudo-pass playbooks/$target
```
